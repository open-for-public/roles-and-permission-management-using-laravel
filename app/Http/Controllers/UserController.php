<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function index()
    {
        $dataUser = DB::table('users')->get();
        return view('akun.index',compact('dataUser'));
    }
}
