<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    public function index()
    {
        $dataRoles = DB::table('roles')->get();

    }

    public function create()
    {

    }

    public function post(Request $request)
    {
        $role = Role::create(['name' => $request->namaRoles]);

        return back();
    }
}
